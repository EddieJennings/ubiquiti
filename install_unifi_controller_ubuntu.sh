#!/usr/bin/bash

# Had to change shebang's directory.  Odd coming from the RHEL world :).

# Install Script for Unifi Controller for Ubuntu 21.04

# Credit to this blog post for helping give a needed clue
# https://www.scheh.com/index.php/2020/10/20/use-unifi-with-mongodb-above-4-0-0/

# Remember to change the variables as needed.
# Remember to validate and test before using in a production environment.

# Set variables

MONGO_DL="https://repo.mongodb.org/apt/ubuntu/dists/focal/mongodb-org/5.0/multiverse/binary-amd64/mongodb-org-server_5.0.2_amd64.deb"
UNIFI_DL="https://dl.ui.com/unifi/6.2.26/unifi_sysvinit_all.deb"
MONGO_FILE="/tmp/$(echo $MONGO_DL | rev | cut -d '/' -f 1 | rev)"
UNIFI_FILE="/tmp/$(echo $UNIFI_DL | rev | cut -d '/' -f 1 | rev)"
INSTALLER_LOG_FILE="/tmp/unifi_installer_$(date +%F).log"
WAITING_MESSAGE="This may take a moment."

# Start the work

echo "#############################################################"
echo "# Welcome to the Ubiquiti Unifi Controller Installer Script #"
echo "# for Ubuntu 21.04.                                         #"
echo "#############################################################"
echo ""

# Check for root privileges

if [ "$(whoami)" != "root" ]; then
	echo "You must run this script using sudo or"
	echo "be logged in as root.  Exiting."
	exit 1
fi

echo "Installing necessary dependencies via apt."
echo $WAITING_MESSAGE
echo ""

apt install -y wget openjdk-8-jre-headless jsvc binutils > $INSTALLER_LOG_FILE 2>&1

echo "Checking for Mongodb installer file in the /tmp directory, and downloading if necessary."
if [ -f "$MONGO_FILE" ]
	then
		echo "$MONGO_FILE was found. Continuing."
		echo ""
	else
		echo "$MONGO_FILE was not found.  Downloading now."
		echo "Downloading Mongodb Community installer. $(echo $WAITING_MESSAGE)"
		echo ""
		wget -O $MONGO_FILE  $MONGO_DL 2>> $INSTALLER_LOG_FILE
		
fi

echo "Checking for Unifi installer file in the /tmp directory, and downloading if necessary."
if [ -f "$UNIFI_FILE" ]
	then
		echo "$UNIFI_FILE was found. Continuing."
		echo ""
	else
		echo "$UNIFI_FILE was not found.  Downloading now."
		echo "Downloading the Unifi Controller installer. $(echo $WAITING_MESSAGE)"
		echo ""
		wget -O $UNIFI_FILE $UNIFI_DL 2>> $INSTALLER_LOG_FILE
		
fi


echo "Installing Mongodb."
echo $WAITING_MESSAGE
echo ""

dpkg -i $MONGO_FILE >> $INSTALLER_LOG_FILE 2>&1

echo "Installing Unifi"
echo $WAITING_MESSAGE
echo ""

dpkg --ignore-depends=mongodb-org-server -i $UNIFI_FILE >> $INSTALLER_LOG_FILE 2>&1

echo ""

echo "################" | tee -a $INSTALLER_LOG_FILE
echo "# **Reminder** #" | tee -a $INSTALLER_LOG_FILE
echo "################" | tee -a $INSTALLER_LOG_FILE
echo "" | tee -a $INSTALLER_LOG_FILE
echo "##############################################################" | tee -a $INSTALLER_LOG_FILE
echo "# Edit the /var/lib/dpkg/status file, and remove the         #" | tee -a $INSTALLER_LOG_FILE
echo "# 'mongodb-org-server (<< 4.0.0)' requirement in the depends #" | tee -a $INSTALLER_LOG_FILE
echo "# section as shown below.                                    #" | tee -a $INSTALLER_LOG_FILE
echo "##############################################################" | tee -a $INSTALLER_LOG_FILE
echo ""  | tee -a $INSTALLER_LOG_FILE
grep -A 10 -i "Package: unifi" /var/lib/dpkg/status | grep --color=always -B 10 -i "mongodb-org-server (<< 4.0.0)"  | tee -a $INSTALLER_LOG_FILE
echo ""

echo "All done!  See $INSTALLER_LOG_FILE for details."
